/**
 * @file js.accordionProcess
 * @since 20170724
 * @author katsushi.ando
 *
 * アコーディオンの処理
 *
 */
var accordionProcess = (function(){

  var accordionProcess = function(obj){
    //引数の部品名を代入
    //this.stepList = obj.stepList;
    //this.apiList = obj.apiList;
    return this.vue();
  };

  var p = accordionProcess.prototype;

  p.vue = function(){

    return new Vue({
      el: '#content',
      data: {
        stepList: "",
        apiList: "",
        apiDetailList: "",
        accessTkun:"",
        httpMethodSelected:"get",
        httpMethodOptions: [
            { text:"GET",  value:"get"},
            { text:"POST", value:"post"}
        ],
        contentTypeSelected:"application/json",
        contentTypeOptions:[
            { text:"Json", value:"application/json"},
            { text:"Text", value:"text/plain"},
            { text:"Html", value:"text/html"},
            { text:"JavaScript", value:"text/javascript"}
        ],
        requestUrl:"",
        isRequestBodyShow:false,
        isListPossibleShow:false
        //コンストラクタ（newした時に呼ばれる処理）		
      },created:function(){
        var self = this;
        var stepListPath = "json/stepList.json";
        var apiListPath = "json/apiList.json";
        var apiListPath = "json/apiDetailList.json";

        Promise.resolve().then(function() {

          return new Promise(function(resolve,reject) {

            //デフォルトの部品の引数が記載しているjsonを読み込み
            var xhr = new XMLHttpRequest();
            xhr.open("get",stepListPath);
            xhr.send();
            xhr.addEventListener("load",function(){
              if(xhr.statusText === "OK"){ 
                //成功処理
                //初期表示処理：部品の引数の設定（ブラウザのストレージに保存している場合はその値を優先とする。）
                self.stepList = JSON.parse(xhr.responseText);
                resolve({"stepList":self.stepList});
              }else{
                //失敗処置
                alert("デフォルトのJsonファイルが存在しません。 パス:" + xhr.responseURL);
                //初期表示処理：部品の引数を空文字に設定
                self.stepList = "";
                reject(self.stepList);
              }
            })
          })

        }).then(function(obj) {

          return new Promise(function(resolve,reject) {

            var xhr2 = new XMLHttpRequest();
            xhr2.open("get",apiListPath);
            xhr2.send();
            xhr2.addEventListener("load",function(){

              if(xhr2.statusText === "OK"){ 
                //成功処理
                //初期表示処理：部品の引数の設定（ブラウザのストレージに保存している場合はその値を優先とする。）
                self.apiList = JSON.parse(xhr2.responseText);
                resolve({"stepList":obj.stepList,"apiList":self.apiList});
              }else{
                //失敗処置
                alert("デフォルトのJsonファイルが存在しません。 パス:" + xhr2.responseURL);
                //初期表示処理：部品の引数を空文字に設定
                self.apiList = "";
                reject(self.apiList);
              }
            })
          })
        }).then(function(obj) {

          return new Promise(function(resolve,reject) {

            var xhr3 = new XMLHttpRequest();
            xhr3.open("get",apiListPath);
            xhr3.send();
            xhr3.addEventListener("load",function(){

              if(xhr3.statusText === "OK"){ 
                //成功処理
                //初期表示処理：部品の引数の設定（ブラウザのストレージに保存している場合はその値を優先とする。）
                self.apiDetailList = JSON.parse(xhr3.responseText);
                resolve({"stepList":obj.stepList,"apiList":self.apiList,"apiDetailList":self.apiDetailList});
              }else{
                //失敗処置
                alert("デフォルトのJsonファイルが存在しません。 パス:" + xhr3.responseURL);
                //初期表示処理：部品の引数を空文字に設定
                self.apiDetailList = "";
                reject(self.apiDetailList);
              }
            })
          })
        }).then(function(obj) {

          //初期表示でstep1が開いている状態にする処理
          obj.stepList[2].contents.maxHeight = obj.stepList[1].contents.openHeight;
          obj.stepList[2].accordion_icon.transform = "rotate(90deg)";
        }).catch(function(err){
          console.log(err);
        });

      },methods:{
        stepListToggle: function(index){
          var self = this;
          this.stepList[index].contents.isActive = !this.stepList[index].contents.isActive;					

          this.stepList.forEach(function(value,count){						
            if(index === count){
              //選択されたstepを開く
              if(self.stepList[index].contents.isActive){
                self.stepList[index].contents.maxHeight = self.stepList[index].contents.openHeight;
                self.stepList[index].accordion_icon.transform = "rotate(90deg)";
              }else{
                //選択されて開いているstepは閉じない
              }
            }else if(index !== count) {
              //選択されていないstepは閉じる
              self.stepList[count].contents.maxHeight = 0;
              self.stepList[count].accordion_icon.transform = "rotate(0deg)";
              self.stepList[count].contents.isActive = false;
            }
          })
        },
        qpiListToggle: function(index){
          this.apiList[index].contents.isActive = !this.apiList[index].contents.isActive;
          if(this.apiList[index].contents.isActive){
            this.apiList[index].contents.maxHeight = this.$refs.contentsInner[index].clientHeight;
            this.apiList[index].accordion_icon.transform = "rotate(90deg)";
          }else{
            this.apiList[index].contents.maxHeight = 0;
            this.apiList[index].accordion_icon.transform = "rotate(0deg)";
          }
        },
        qpiDetailListToggle: function(index){
          this.apiDetailList[index].contents.isActive = !this.apiDetailList[index].contents.isActive;
          if(this.apiDetailList[index].contents.isActive){
            this.apiDetailList[index].contents.maxHeight = this.$refs.contentsInner[index].clientHeight;
            this.apiDetailList[index].accordion_icon.transform = "rotate(90deg)";
          }else{
            this.apiDetailList[index].contents.maxHeight = 0;
            this.apiDetailList[index].accordion_icon.transform = "rotate(0deg)";
          }
        },
        myFilter:function(){
            this.isActive = !this.isActive;
        }
      }
    });
  }
  return accordionProcess;
})()
